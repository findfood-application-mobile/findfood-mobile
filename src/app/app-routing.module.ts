import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/authentification/auth-guard.service';

const routes: Routes = [
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'edit-profile', loadChildren: './pages/user/edit-profile/edit-profile.module#EditProfilePageModule' },
  { path: 'all-restaurant', loadChildren: './pages/restaurant/all-restaurant/all-restaurant.module#AllRestaurantPageModule' },
  { path: 'description/:id', loadChildren: './pages/restaurant/description/description.module#DescriptionPageModule' },
  { path: 'search-filter', loadChildren: './pages/restaurant/search-filter/search-filter.module#SearchFilterPageModule' },
  { path: 'result-search/:speciality', loadChildren: './pages/restaurant/result-search/result-search.module#ResultSearchPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuardService] },
  { path: 'top-restaurant', loadChildren: './pages/restaurant/top-restaurant/top-restaurant.module#TopRestaurantPageModule' },
  { path: 'modal-img', loadChildren: './pages/restaurant/modal-img/modal-img.module#ModalImgPageModule' },
  { path: 'comments', loadChildren: './pages/restaurant/description/comments/comments.module#CommentsPageModule' },
  { path: 'details', loadChildren: './pages/restaurant/description/details/details.module#DetailsPageModule' },
  { path: 'location', loadChildren: './pages/restaurant/description/location/location.module#LocationPageModule' },
  { path: 'add-restaurant', loadChildren: './pages/restaurant/add-restaurant/add-restaurant.module#AddRestaurantPageModule' },
  { path: 'contact', loadChildren: './pages/contact/contact.module#ContactPageModule' },
  { path: 'forgot-password', loadChildren: './pages/user/forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'update-password', loadChildren: './pages/user/update-password/update-password.module#UpdatePasswordPageModule' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
