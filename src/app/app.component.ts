import { Component } from '@angular/core';

import { Platform, NavController, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Pages } from './interfaces/pages';
import { Storage } from '@ionic/storage';
import { AuthService } from './services/authentification/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public appPages: Array<Pages>;
  nom: string;
  prenom: null;
  ville: null;
  userInfo: null;
  mail: string;
  username: null;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    public storage: Storage,
    public AuthService: AuthService,
    public Router: Router,
    public events5: Events,
  ) {
    this.events5.subscribe('info', (data) => {
      this.mail = data[0].mail;
      this.username = data[0].username;
    });
    this.storage.get('USER_INFO').then((response) => {
      if (response) {
        console.log(response);
        this.username = response[0].username;
        this.mail = response[0].mail
      }
    });
    this.appPages = [
      {
        title: 'Accueil',
        url: '/home',
        direct: 'root',
        icon: 'home'
      },
      {
        title: 'Liste des restaurants',
        url: '/all-restaurant',
        direct: 'forward',
        icon: 'restaurant'
      },
      {
        title: 'Top 10 restaurants',
        url: '/top-restaurant',
        direct: 'forward',
        icon: 'trophy'
      },
      {
        title: 'Ajouter un restaurant',
        url: '/add-restaurant',
        direct: 'forward',
        icon: 'add-circle'
      },
      {
        title: 'Contactez-nous',
        url: '/contact',
        direct: 'forward',
        icon: 'mail'
      },
/*
      {
        title: 'A propos',
        url: '/about',
        direct: 'forward',
        icon: 'information-circle-outline'
      },

       {
        title: 'Paramètres',
        url: '/settings',
        direct: 'forward',
        icon: 'cog'
      } */
    ];

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 1000);
      this.AuthService.authState.subscribe(state => {
        if (state) {
          this.Router.navigate(['/home']);
        } else {
          this.Router.navigate(['/login']);
        }
      });
    }).catch(() => { });
  }

  goToEditProfile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  logout() {
    this.AuthService.logout();
  }

}
