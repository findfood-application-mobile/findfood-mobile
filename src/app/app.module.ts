import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';

// Modal Pages
import { DescriptionPageModule } from './pages/restaurant/description/description.module';
import { SearchFilterPageModule } from './pages/restaurant/search-filter/search-filter.module';

// Components
import { NotificationsComponent } from './components/notifications/notifications.component';
import { IonicStorageModule } from '@ionic/storage';

// Services
import { AuthGuardService } from './services/authentification/auth-guard.service';
import { AuthService } from './services/authentification/auth.service';
import { ModalImgPage } from './pages/restaurant/modal-img/modal-img.page';


import { Camera } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@NgModule({
  declarations: [AppComponent,NotificationsComponent, ModalImgPage],
  entryComponents: [NotificationsComponent, ModalImgPage],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot({
      mode: 'ios'
    }),
    AppRoutingModule,
    HttpClientModule,
    DescriptionPageModule,
    SearchFilterPageModule,
    IonicStorageModule.forRoot(),
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AuthGuardService,
    AuthService,
    Camera,
    File,
    WebView,
    FilePath,
    Keyboard,
    NativePageTransitions,
    Geolocation
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
