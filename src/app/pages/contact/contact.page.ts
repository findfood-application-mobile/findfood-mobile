import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/rest/rest.service';
import { FunctionService } from 'src/app/services/function/function.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  firstname: string;
  lastname: string;
  email: string;
  object: string;
  message: string;

  constructor(
    public apiService: ApiService,
    public functionService: FunctionService,
    public router: Router,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
  }

  async send() {
    if (this.firstname && this.lastname && this.email && this.object && this.message) {
      let param = {
        'firstname': this.firstname,
        'lastname': this.lastname,
        'email': this.email,
        'object': this.object,
        'message': this.message,
        'type': 'contact',
      }
      const loading = await this.loadingController.create({
        cssClass: 'customLoadClass',
        message: "chargement",
        spinner: 'dots'
      });
      await loading.present();
      this.apiService.sendMail(param).subscribe((resp: any) => {
        switch (resp.status) {
          case 1:
            this.functionService.presentToast("Email envoyé avec succées");
            this.router.navigate(["/home"]);
            break;
          case 0:
            this.functionService.presentToast("Email envoyé avec succées");
            this.router.navigate(["/home"]);
            break;
          default:
            this.functionService.presentToast("Une erreur s'est produite durant l'envoie de votre mail. Verifier votre connexion.");
            break;
        }
        loading.dismiss();
      })
    } else {
      this.functionService.presentToast("Veuillez remplir tout les champs.")
    }
  }

}
