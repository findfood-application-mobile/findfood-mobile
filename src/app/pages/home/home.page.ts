import { Component, ViewChild, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  ModalController,
  IonInfiniteScroll,
  LoadingController
} from '@ionic/angular';
// Modals
import { SearchFilterPage } from '../../pages/restaurant/search-filter/search-filter.page';
// Call notifications test by Popover and Custom Component.
import { NotificationsComponent } from './../../components/notifications/notifications.component';
import { ApiService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';
import { Events } from '@ionic/angular';
import { Router } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { FunctionService } from 'src/app/services/function/function.service';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AuthService } from 'src/app/services/authentification/auth.service';
import { bubble } from 'src/app/services/animations/animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  animations: [ bubble ]
})
export class HomePage implements OnInit {
  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;

  url = this.apiService.url;
  urlImg = this.apiService.urlImg;
  currentId: number;
  searchKey = '';
  userLocation: any;
  dataList = [];
  slideConfig = {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 1.6,
    initialSlide: 3,
  }
  proximityResto: any;
  token: any;
  specialities: any;
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
  };
  bool = false;
  userGeolocation: String;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private apiService: ApiService,
    public storage: Storage,
    public events1: Events,
    private router: Router,
    private keyboard: Keyboard,
    public functionService: FunctionService,
    private nativePageTransitions: NativePageTransitions,
    public loadingController: LoadingController,
    public geolocation: Geolocation,
    private AuthService: AuthService
  ) { }


  ngOnInit() {
    this.loadData();
  }

  async getLocation() {
    const loading = await this.loadingController.create({
      cssClass:'customLoadClass',
      message: 'chargement',
      spinner: 'dots'
    });
    await loading.present();
    this.geolocation.getCurrentPosition().then((resp) => {
      this.apiService.getLocation(resp.coords.latitude, resp.coords.longitude).subscribe((data : any)=>{
        this.userLocation = data.results[0].address_components[2].long_name;
        this.functionService.presentToast("Nouvelle localisation : " + this.userLocation)
        this.getProximityResto();
      })
     }).catch((error) => {
      this.functionService.presentToast("Erreur nous ne parvenons pas à vous localiser.")
     });
     loading.dismiss();
  }

  async loadData(): Promise<void> {
    const loading = await this.loadingController.create({
      cssClass:'customLoadClass',
      message: "chargement",
      spinner: 'dots'
    });
    await loading.present();
    this.storage.get('USER_INFO').then((response) => {
    this.currentId = response[0].id;
    this.userLocation = response[0].ville;
    this.token = response[0].token;
    this.getSpeciality();
    this.displayResto();
    this.getProximityResto()
    loading.dismiss();
    });
  }

  
  goToDescription(param) {
    this.nativePageTransitions.slide(this.options);
    this.navCtrl.navigateForward('/description/' + param)
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getSpeciality();
      this.displayResto();
      event.target.complete();
    }, 2000);
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  async ionViewDidLeave() {
  this.AuthService.checkLogged();
  this.bool ? this.events1.publish('search', this.searchKey) : this.events1.publish('search', "");
  this.bool = false;
  this.searchKey = ''
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: "Changer la localisation",
      inputs: [
        {
          name: 'location',
          placeholder: 'Saisissez une localisation.',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
          }
        },
        {
          text: 'Valider',
          handler: async (data) => {
            this.userLocation = data.location;
            this.functionService.presentToast("Votre localisation à bien été changer.")
            this.getProximityResto();
          }
        }
      ]
    });
    changeLocation.present();
  }

  async searchFilter() {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }

  async notifications(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

  displayResto() {
    this.apiService.getFavorite(this.currentId).subscribe((data: any) => {
      this.dataList = data.data;
    });
  }

  getSpeciality() {
    this.apiService.getAllSpeciality().subscribe((data: any) => {
      this.specialities = data.data;
    })
  }

  getProximityResto() {
    let param = {
      'city': this.userLocation,
    }
    this.apiService.searchByCriteria(param).subscribe((resp: any) => {
      resp.status == 1 ? this.proximityResto = resp.data : this.proximityResto = "";
    })
  }

  setResearch() {
    if (this.searchKey) {
      this.bool = true;
      this.keyboard.hide();
      this.router.navigate(['/result-search', ""]);
    }
  }

  removeFav(id: number) {
    var datas = {
      'resto_id': id,
      'id': this.currentId,
      'token': this.token,
    };
    this.apiService.removeToFavorite(datas).subscribe((data) => {
      this.displayResto();
    });
  }

  openSpeciality(speciality) {
    this.apiService.searchByCriteria({ 'speciality': speciality }).subscribe((resp: any) => {
    })
  }

}
