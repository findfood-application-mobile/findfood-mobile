import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController, Events } from '@ionic/angular';
import { ApiService } from 'src/app/services/rest/rest.service';
import { FunctionService } from 'src/app/services/function/function.service';
import { Storage } from '@ionic/storage';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from 'src/app/services/authentification/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public onLoginForm: FormGroup;

  dataList = [];
  username : string;
  password : string;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    public services: FunctionService,
    public storage: Storage,
    public router: Router,
    public AuthService: AuthService,
    public events6: Events
  ) { }

  checkLogin() {
    let params = {
      'username' : this.onLoginForm.get('username').value,
      'password' : this.onLoginForm.get('password').value,
    }
    this.apiService.connection(params).subscribe((data) => {
      console.log(data);
      var isLogged = data['isLogged'];
      var level = data['level'];
      if(isLogged == true) {
        this.storage.ready().then(() => {
          var datas = [{
            'id': data['info'].id_user,
            'username': data['info'].username,
            'level': data['info'].rang,
            'nom': data['info'].nom,
            'prenom': data['info'].prenom,
            'ville': data['info'].ville_domicile,
            'token': data['info'].token,
            'mail': data['info'].mail,
          }];
          this.events6.publish('info', datas);
          this.AuthService.login(datas);
        });
      }
       else {
        switch(level) {
          case 1:
            this.services.presentToast('Erreur : Impossible de vous identifier. Merci de verifier votre login et/ou mot de passe.');
            break;
          case 2:
            this.services.presentToast('Erreur : Impossible de vous identifier. Merci de verifier votre login et/ou mot de passe.');
            break;
          case 4:
            this.services.presentToast('Erreur : Votre compote est bloqué suite à de nombreux essais infructueux. Veuillez reinitialisez votre mot de passe.');
            break;
          default:
            this.services.presentToast('Erreur : Aucun champs n\'a été remplis.');
        }
      }
    });
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.onLoginForm = this.formBuilder.group({
      'username': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  async forgotPass() {
    const alert = await this.alertCtrl.create({
      header: 'Mot de passe oublier?',
      message: 'Entrez votre adresse e-mail pour recevoir un lien de réinitialisation de votre mot de passe.',
      inputs: [
        {
          name: 'username',
          type: 'text',
          placeholder: 'Nom d\'utilisateur'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: async () => {
            const loader = await this.loadingCtrl.create({
              duration: 2000
            });

            loader.present();
            loader.onWillDismiss().then(async l => {
              const toast = await this.toastCtrl.create({
                showCloseButton: true,
                message: 'Email was sended successfully.',
                duration: 3000,
                position: 'bottom'
              });

              toast.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  goToRegister() {
    //this.navCtrl.navigateRoot('/register');
    this.router.navigate(['/register'])
  }

  goToPassword() {
    //this.navCtrl.navigateRoot('/forgot-password');
    this.router.navigate(['/forgot-password'])
  }

}
