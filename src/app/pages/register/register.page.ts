import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/rest/rest.service';
import { FunctionService } from 'src/app/services/function/function.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public onRegisterForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    public apiService: ApiService,
    public functionService: FunctionService,
    public loadingController: LoadingController,
    public router: Router
  ) { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.onRegisterForm = this.formBuilder.group({
      'username': [null, Validators.compose([
        Validators.required
      ])],
      'lastname': [null, Validators.compose([
        Validators.required
      ])],
      'firstname': [null, Validators.compose([
        Validators.required
      ])],
      'birthday': [null, Validators.compose([
        Validators.required
      ])],
      'email': [null, Validators.compose([
        Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)
      ])],
      'password1': [null, Validators.compose([
        Validators.required
      ])],
      'password2': [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  goToLogin() {
    this.navCtrl.navigateBack(['/login']);
  }

  goToRegister() {
    if (this.onRegisterForm.get('password1').value == this.onRegisterForm.get('password2').value) {
      const dateObject = new Date(this.onRegisterForm.get('birthday').value);
      dateObject.setDate(dateObject.getDate() + 1);
      const dateString = dateObject.toISOString().substring(0, 10);
      var params = {
        'username': this.onRegisterForm.get('username').value,
        'nom': this.onRegisterForm.get('lastname').value,
        'prenom': this.onRegisterForm.get('firstname').value,
        'birthday': dateString,
        'mail': this.onRegisterForm.get('email').value,
        'password': this.onRegisterForm.get('password1').value
      }
      this.apiService.registerUser(params).subscribe((data: any) => {
        if (data.exist == true) {
          this.functionService.presentToast('Erreur : Ce nom d\'utilisateur est déja pris.');
        }
        else {
          this.sendMail();
        }
      });
    }
    else {
      this.functionService.presentToast('Erreur : Le mot de passe ne correspond pas.');
    }
  }

  async sendMail(): Promise<void> {
    const loading = await this.loadingController.create({
      cssClass: 'customLoadClass',
      message: "chargement",
      spinner: 'dots'
    });
    let param = {
      'username': this.onRegisterForm.get('username').value,
      'email': this.onRegisterForm.get('email').value,
      'firstname': this.onRegisterForm.get('firstname').value,
      'type': 'subscription',
    }
    await loading.present();
    this.apiService.sendMail(param).subscribe((resp: any) => {
      loading.dismiss();
      this.navCtrl.navigateRoot('/login');
      this.functionService.presentToast('Inscription réalisée avec succès.');
    });
  }

  ValidateEmail(mail) {
    let check = false;
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test("test@test.com")) {
      check = true;
    }
    return check;
  }
}