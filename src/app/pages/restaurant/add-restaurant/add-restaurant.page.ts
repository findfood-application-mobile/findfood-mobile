import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/rest/rest.service';
import { FunctionService } from 'src/app/services/function/function.service';
import { Router } from '@angular/router';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { HttpClient } from '@angular/common/http';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';

import { finalize } from 'rxjs/operators';


@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.page.html',
  styleUrls: ['./add-restaurant.page.scss'],
})
export class AddRestaurantPage implements OnInit {

  listSpeciality: any;
  specialite: any;
  name: any;
  prix1: any;
  prix2: any;
  dnms: any;
  num_rue: any;
  nom_rue: any;
  ville: any;
  cp: any;
  images = [];
  username: string;
  imgstore = [];
  cptUpload: number = 0;
  listCity: any;

  constructor(
    private apiService: ApiService,
    private functionService: FunctionService,
    private router: Router,
    private camera: Camera, private file: File, private http: HttpClient, private webview: WebView,
    private actionSheetController: ActionSheetController, private toastController: ToastController,
    private storage: Storage, private plt: Platform, private loadingController: LoadingController,
    private ref: ChangeDetectorRef,
    private filePath: FilePath
  ) { }

  ngOnInit() {
    this.getUserInfo();
    this.getSpecialityList();
    this.plt.ready().then(() => {
      this.loadStoredImages();
    });
  }

  refresh() {
    this.ref.detectChanges();
  }

  getCity(cp) {
    let param = {
      'cp': cp
    }
    this.apiService.getCityByCp(param).subscribe((resp: any) => {
      this.listCity = resp.data;
    })
    this.ville = "";
  }

  getUserInfo() {
    this.storage.get('USER_INFO').then((response) => {
      this.username = response[0].username;
    });
  }

  loadStoredImages() {
    this.storage.get("STORAGE_KEY").then(images => {
      if (images) {
        let arr = JSON.parse(images);
        this.images = [];
        for (let img of arr) {
          let filePath = this.file.dataDirectory + img;
          let resPath = this.pathForImage(filePath);
          this.images.push({ name: img, path: resPath, filePath: filePath });
        }
      }
    });
  }

  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Selection des images",
      buttons: [{
        text: 'A partir de la bibliothèque',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Prendre une photo',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Annuler',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  takePicture(sourceType: PictureSourceType) {
    var options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(imagePath => {
      if (this.plt.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    });

  }

  createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.updateStoredImages(newFileName);
    }, error => {
      this.functionService.presentToast("Erreur lors de l'upload du fichier.");
    });
  }

  updateStoredImages(name) {
    this.storage.get("STORAGE_KEY").then(images => {
      let arr = JSON.parse(images);
      if (!arr) {
        let newImages = [name];
        this.storage.set("STORAGE_KEY", JSON.stringify(newImages));
      } else {
        arr.push(name);
        this.storage.set("STORAGE_KEY", JSON.stringify(arr));
      }

      let filePath = this.file.dataDirectory + name;
      let resPath = this.pathForImage(filePath);

      let newEntry = {
        name: name,
        path: resPath,
        filePath: filePath
      };

      this.images = [newEntry, ...this.images];
      this.ref.detectChanges(); // trigger change detection cycle
    });
  }

  deleteImage(imgEntry, position) {
    this.images.splice(position, 1);

    this.storage.get("STORAGE_KEY").then(images => {
      let arr = JSON.parse(images);
      let filtered = arr.filter(name => name != imgEntry.name);
      this.storage.set("STORAGE_KEY", JSON.stringify(filtered));

      var correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);

      this.file.removeFile(correctPath, imgEntry.name).then(res => {
        this.functionService.presentToast("Fichier supprimé.");
      });
    });
  }

  startUpload(imgEntry) {
    this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry => {
        (<FileEntry>entry).file(file => this.readFile(file))
      })
      .catch(err => {
        this.functionService.presentToast("Erreur lors de la lecture du fichier.");
      });
  }

  readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
        type: file.type
      });
      formData.append('file', imgBlob, file.name);
      formData.append('user', this.username);
      this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }

  async uploadImageData(formData: FormData) {
    const loading = await this.loadingController.create({
      message: "Upload de l'image...",
    });
    await loading.present();

    this.apiService.uploadFile(formData)
      .pipe(
        finalize(() => {
          loading.dismiss();
        })
      )
      .subscribe(res => {
        if (res['status'] == 1) {
          this.functionService.presentToast("Upload du fichier réalisé avec succés.")
          this.endUpload();
          this.imgstore = [...this.imgstore,res['img']];

        } else {
          this.functionService.presentToast("L'upload du fichier à échoué.")
        }

      });
  }

  getSpecialityList() {
    this.apiService.getAllSpeciality().subscribe((response: any) => {
      this.listSpeciality = response.data;
    })
  };

  checkForm() {
    if (this.name && this.specialite && this.prix1 && this.prix2 && this.num_rue && this.nom_rue && this.cp && this.ville) {
      return true;
    }
    return false;
  }

  addResto() {
    if(this.checkForm()) {
    if(!this.imgstore[3]){
      this.imgstore[3]="null";
    }
    let param =
    {
      "nom_restau": this.name,
      "id_specialite": this.specialite,
      "prix_min": this.prix1,
      "prix_max": this.prix2,
      "num_rue": this.num_rue,
      "nom_rue": this.nom_rue,
      "cp": this.cp,
      "ville": this.ville,
      "denomination_sociale": this.dnms,
      "img1": this.imgstore[0],
      "img2": this.imgstore[1],
      "img3": this.imgstore[2],
      "img4": this.imgstore[3],
    }
    this.apiService.addRestaurant(param).subscribe((resp: any) => {
      console.log(param)
      console.log(resp);
      if (resp.status == 1) {
        this.functionService.presentToast("Restaurant ajouté avec succés. Il sera visible après vérification par l'équipe.");
        this.router.navigate(['/home']);
      }
      else {
        this.functionService.presentToast("Une erreur s'est produite lors de l'ajout de votre restaurant.");
      }
    })
  }
  else{
    this.functionService.presentToast("Veuillez completer les champs manquants")
  }
  }

  endUpload() {
    this.cptUpload++;
    this.refresh();
  }

}
