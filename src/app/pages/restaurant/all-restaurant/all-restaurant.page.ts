import { Component, ViewChild, OnInit } from '@angular/core';
import { IonInfiniteScroll, LoadingController, Events } from '@ionic/angular';
import { ApiService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';
import { bubble } from '../../../services/animations/animations'

@Component({
  selector: 'app-all-restaurant',
  templateUrl: './all-restaurant.page.html',
  styleUrls: ['./all-restaurant.page.scss'],
  animations: [bubble]
})
export class AllRestaurantPage implements OnInit {

  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
  dataList = [];
  allList: number;
  iduser: any;
  urlImg = this.apiService.urlImg;
  specialities: [];
  slideConfig = {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 1.6,
    initialSlide: 3,
  }

  constructor(
    private apiService: ApiService,
    public storage: Storage,
    public loadingController: LoadingController,
    private events1: Events
  ) { }

  ngOnInit() {
    this.loadDatas();
  }

  async loadDatas(): Promise<void> {
    const loading = await this.loadingController.create({
      cssClass: 'customLoadClass',
      message: "chargement",
      spinner: 'dots'
    });
    await loading.present();
    this.getDatas();
    this.getSpeciality();
    this.displayResto();
    loading.dismiss();
  }

  getDatas() {
    this.storage.get('USER_INFO').then((response) => {
      this.iduser = response[0].id;
    });
  }

  getSpeciality() {
    this.apiService.getAllSpeciality().subscribe((data: any) => {
      this.specialities = data.data;
    })
  }

  async ionViewDidLeave() {
    this.events1.publish('search', "");
  }

  displayResto() {
    for (let i = 0; i < 3; i++) {
      this.apiService.getRest().subscribe((data: any) => {
        this.allList = data.data.length;
        this.dataList.push(data.data[i]);
      });
    }
  }

  loadData(event) {
    setTimeout(() => {
      event.target.complete();
      for (let i = 0; i < 1; i++) {
        this.apiService.getRest().subscribe((data: any) => {
          this.dataList.push(data.data[this.dataList.length]);
        });
      }
      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      // Reduce by number data display
      if (this.dataList.length >= this.allList - 1) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
}