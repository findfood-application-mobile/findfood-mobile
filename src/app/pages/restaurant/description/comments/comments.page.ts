import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';
import { FunctionService } from 'src/app/services/function/function.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/authentification/auth.service';
import { LoadingController } from '@ionic/angular';
import { fadeout } from 'src/app/services/animations/animations';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.page.html',
  styleUrls: ['./comments.page.scss'],
  animations: [fadeout]
})
export class CommentsPage implements OnInit {

  idresto: any;
  iduser: number;
  comments: any;
  userComment: string;
  token: any;

  constructor(
    public apiService: ApiService,
    public storage: Storage,
    public services: FunctionService,
    private activatedRoute: ActivatedRoute,
    public functionService: FunctionService,
    public authService: AuthService,
    public loadingController: LoadingController
  ) {
    this.idresto = activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getUserInfo();
    this.getComment();
  }

  getUserInfo() {
    this.storage.get('USER_INFO').then((response) => {
      this.iduser = response[0].id;
      this.token = response[0].token
    });
  }

  getComment() {
    this.apiService.getCommentByResto(this.idresto).subscribe((res: any) => {
      res.status == 1 ? this.comments = res.data : this.comments = "";
    });
  }

  async setComment() {
    console.log(this.userComment);
    if(this.userComment.length > 5) {
    const loading = await this.loadingController.create({
      cssClass: 'customLoadClass',
      message: "chargement",
      spinner: 'dots'
    });
    await loading.present();
    let param = {
      'content': this.userComment,
      'id': this.iduser,
      'id_restaurant': this.idresto,
      'token': this.token,
    };
    this.apiService.setComment(param).subscribe((resp: any) => {
      switch (resp.status) {
        case 1:
          this.functionService.presentToast('Commentaire ajouté avec succés');
          this.userComment = "";
          this.getComment();
          break;
        case -99:
          this.authService.logout();
          this.functionService.presentToast("Une autre session à été ouverte sur un autre appareil. Vous avez été déconnecter.");
          break;
        default:
          this.functionService.presentToast("Une erreur s'est produite lors de l'ajout de votre commentaire.");
          break;
      }
      loading.dismiss();
    });
  } else {
    this.functionService.presentToast("Votre message doit comporter au moins 5 caractères.")
  }
  }

  deleteComment(idcomment) {
    this.apiService.deleteComment(idcomment, this.iduser, this.token).subscribe((resp: any) => {
      switch (resp.status) {
        case 1:
          this.functionService.presentToast('Commentaire supprimé avec succés');
          this.getComment();
          break;
        case -99:
          this.authService.logout();
          this.functionService.presentToast('Une autre session à été ouverte sur un autre appareil. Vous avez été déconnecter.');
          break;
        default:
          this.functionService.presentToast("Une erreur s'est produite lors de la suppresion de votre commentaire.");
          break;
      }
    });
  }

}
