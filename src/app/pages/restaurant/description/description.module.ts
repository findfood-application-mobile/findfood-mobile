import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DescriptionPage } from './description.page';

import { DetailsPage } from './details/details.page';
import { LocationPage } from './location/location.page';
import { CommentsPage } from './comments/comments.page';

const routes: Routes = [
  {
    path: '',
    component: DescriptionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DescriptionPage, DetailsPage, LocationPage, CommentsPage]
})
export class DescriptionPageModule {}
