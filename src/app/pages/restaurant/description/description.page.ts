import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NavController, Events, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';
import { FunctionService } from 'src/app/services/function/function.service';
import { ModalImgPage } from '../modal-img/modal-img.page';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/authentification/auth.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { fadeout } from 'src/app/services/animations/animations';

@Component({
  selector: 'app-description',
  templateUrl: './description.page.html',
  styleUrls: ['./description.page.scss'],
  animations: [fadeout]
})

export class DescriptionPage implements OnInit {
  @Input() id: number;
  idresto: any;
  datas: any;
  urlImg = this.apiService.urlImg;
  iduser: number;
  favoriteList = [];
  isAlreadyFav = false;
  token: string;
  selectedSegment: string;
  search: any;

  constructor(
    public apiService: ApiService,
    public storage: Storage,
    public services: FunctionService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    public events: Events,
    public loadingController: LoadingController,
    public authService: AuthService
  ) {
    this.idresto = activatedRoute.snapshot.paramMap.get('id');
    this.selectedSegment = 'Détails';
    this.loadData();
  }

  ngOnInit() {
    //this.loadData();
  }

  async loadData(): Promise<void> {
    const loading = await this.loadingController.create({
      cssClass: 'customLoadClass',
      message: "chargement",
      spinner: 'dots'
    });
    await loading.present();
    this.getUserInfo();
    this.getInfoResto();
    this.events.subscribe('previous', (data) => {
      this.search = data;
      this.events.unsubscribe('previous')
    });
    loading.dismiss();
  }

  previous() {
    switch (this.search) {
      case "":
        console.log("1er");
        this.navCtrl.navigateBack(['/result-search', ""])
        break;
      case true:
          console.log("2eme");
        this.navCtrl.navigateBack(['/result-search', this.search])
        break;
      default:
          console.log("3eme");
        this.navCtrl.back();
        break;
    }
  }

  getUserInfo() {
    this.storage.get('USER_INFO').then((response) => {
      if (response) {
        this.iduser = response[0].id;
        this.token = response[0].token;
        this.checkFav();
      }
    });
  }

  segmentChosen(name) {
    this.selectedSegment = name;
  }

  getInfoResto() {
    this.apiService.getRestoById(this.idresto).subscribe((data: any) => {
      this.datas = data.data[0];
    });
  }

  setFav(id: number) {
    var datas = {
      'resto_id': id,
      'id': this.iduser,
      'token': this.token
    };
    this.apiService.addToFavorite(datas).subscribe((data: any) => {
      switch (data.status) {
        case 1:
          this.services.presentToast('Restaurant ajouter à vos favoris avec succées.');
          this.isAlreadyFav = !this.isAlreadyFav;
          break;
        case -99:
          this.authService.logout();
          this.services.presentToast('Une autre session à été ouverte sur un autre appareil. Vous avez été déconnecter.');
          break;
        default:
          this.services.presentToast("Une erreur s'est produite. Veuillez réessayer ou verifier votre connexion");
          break;
      }
    });
  }

  removeFav(id: number) {
    var datas = {
      'resto_id': id,
      'id': this.iduser,
      'token': this.token
    };
    this.apiService.removeToFavorite(datas).subscribe((data: any) => {
      switch (data.status) {
        case 1:
          this.services.presentToast('Restaurant supprimer de vos favoris avec succées.');
          this.isAlreadyFav = !this.isAlreadyFav;
          break;
        case -99:
          this.authService.logout();
          this.services.presentToast('Une autre session à été ouverte sur un autre appareil. Vous avez été déconnecter.');
          break;
        default:
          this.services.presentToast("Une erreur s'est produite. Veuillez réessayer ou verifier votre connexion");
          break;
      }
    });
  }

  checkFav() {
    this.apiService.getIdFavorite(this.iduser).subscribe((data: any) => {
      if (data.data != null) {
        this.favoriteList = (data.data).split(",");
        this.checkInList()
      }
    });
  }

  checkInList() {
    if (this.favoriteList) {
      if (this.favoriteList.includes((this.idresto))) {
        this.isAlreadyFav = true;
      }
    }
  }
}