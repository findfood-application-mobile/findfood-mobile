import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ApiService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';
import { FunctionService } from 'src/app/services/function/function.service';
import { ModalImgPage } from '../../modal-img/modal-img.page';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/authentification/auth.service';
import { fadeout } from 'src/app/services/animations/animations';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
  animations: [fadeout]
})

export class DetailsPage implements OnInit {
  @Input() id: number;
  @Input() nom: string;
  @Input() rating: number;
  @Output() ratingChange: EventEmitter<number> = new EventEmitter();
  idresto: any;
  nomresto: any;
  datas: any;
  urlImg = this.apiService.urlImg;
  img: any;
  iduser: number;
  token: any;
  COLORS = {
    'GREY': "#E0E0E0",
    'GREEN': "#76FF03",
    'YELLOW': "#FFCA28",
    'RED': "#DD2C00"
  };
  canVote: boolean = false;
  displaybtn: boolean = false;

  constructor(
    private modalCtrl: ModalController,
    public apiService: ApiService,
    public storage: Storage,
    public services: FunctionService,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService
  ) {
    this.idresto = activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getUserInfo();
    this.getInfoResto();
  }

  displayVote(act) {
    act == 1 ? this.displaybtn  = true : this.displaybtn = false 
  }

  async openImg(modalImg) {
    const modal = await this.modalCtrl.create({
      component: ModalImgPage,
      componentProps: {
        'img': modalImg,
      }
    });
    return await modal.present();
  }

  rate(index: number) {
    this.rating = index
    this.ratingChange.emit(this.rating)
  }

  setRate() {
    let params = {
      'resto_id': this.idresto,
      'id': this.iduser,
      'rating': this.rating,
      'token': this.token
    };
    this.apiService.addVote(params).subscribe((data: any) => {
      console.log(data);
      switch (data.status) {
        case 1:
          this.services.presentToast('Votre vote à bien été pris en compte.');
          this.canVote = !this.canVote;
          this.displaybtn = false;
          this.getInfoResto();
          break;
        case -99:
          this.authService.logout();
          this.services.presentToast('Une autre session à été ouverte sur un autre appareil. Vous avez été déconnecter.');
          break;
        default:
          this.services.presentToast("Une erreur s'est produite. Veuillez réessayer ou verifier votre connexion");
          break;
      }
    });
  }

  getColor(index: number) {
    if (this.isAboveRating(index)) {
      return "#E0E0E0";
    }
    switch (this.rating) {
      case 1:
      case 2:
        return this.COLORS.GREEN;
      case 3:
        return this.COLORS.YELLOW;
      case 4:
      case 5:
        return this.COLORS.RED;
      default:
        return this.COLORS.GREY;
    }
  }

  isAboveRating(index: number): boolean {
    return index > this.rating;
  }

  getUserInfo() {
    this.storage.get('USER_INFO').then((response) => {
      this.iduser = response[0].id;
      this.token = response[0].token;
      this.checkVote();
    });
  }

  getInfoResto() {
    this.img = [];
    this.apiService.getRestoById(this.idresto).subscribe((data: any) => {
      this.datas = data.data[0];
      this.img.push(data.data[0].img1)
      this.img.push(data.data[0].img2)
      this.img.push(data.data[0].img3)
      this.img.push(data.data[0].img4)
    });
  }

  checkVote() {
    let params = {
      "id_restau": this.idresto,
      "currentid": this.iduser
    }
    this.apiService.checkVote(params).subscribe((data: any) => {
      if (data.status == 1) {
        this.canVote = true;
      }
    });
  }
}