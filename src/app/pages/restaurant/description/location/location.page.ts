import { Component, OnInit } from '@angular/core';
import { ToastController, Platform, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/rest/rest.service';
import { DomSanitizer } from '@angular/platform-browser';
import { fadeout } from 'src/app/services/animations/animations';

@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
  animations: [fadeout]
})
export class LocationPage implements OnInit {
  idresto : string;
  numrue : number;
  adresse : string;
  ville : string;
  cp : number;
  url : any;
  nom_restau : string;


  constructor(
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private platform: Platform,
    private activatedRoute : ActivatedRoute,
    public apiService : ApiService,
    private sanitizer : DomSanitizer
    ) { 
      this.idresto = activatedRoute.snapshot.paramMap.get('id');
     }

   ngOnInit() {
     this.getInfoResto();
  }
  
  getInfoResto() {
    this.apiService.getRestoById(this.idresto).subscribe((data: any) => {
      this.numrue = data.data[0].num_rue;
      this.ville = data.data[0].ville;
      this.cp = data.data[0].cp;
      this.adresse = data.data[0].nom_rue;
      this.nom_restau = data.data[0].nom_restau;
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.google.com/maps/embed/v1/place?key=AIzaSyBtG8dM2NsBBgorNzPO14IxA-qc0drEE9U&q="+this.numrue+"+"+this.adresse+"+"+this.cp+"+"+this.cp)
    });
  }
}