import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalImgPage } from './modal-img.page';

describe('ModalImgPage', () => {
  let component: ModalImgPage;
  let fixture: ComponentFixture<ModalImgPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalImgPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalImgPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
