import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-modal-img',
  templateUrl: './modal-img.page.html',
  styleUrls: ['./modal-img.page.scss'],
})
export class ModalImgPage implements OnInit {
  @Input() img: string;
  sliderOpts = {
    zoom: {
      maxRatio: 3
    }
  }

  constructor(
    public modalCtrl : ModalController,
    public navParams: NavParams
  ) { }

  ngOnInit() {
    this.img = this.navParams.get('img');
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }
}
