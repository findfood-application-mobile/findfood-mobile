import { Component, OnInit } from '@angular/core';
import { Events, ModalController, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/rest/rest.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { ActivatedRoute } from '@angular/router';
import { fadeout, bubble, move } from 'src/app/services/animations/animations';

@Component({
  selector: 'app-result-search',
  templateUrl: './result-search.page.html',
  styleUrls: ['./result-search.page.scss'],
  animations: [ move, fadeout, bubble]
})
export class ResultSearchPage implements OnInit {
  datasReceive: null;
  dataList = [];
  city: any;
  speciality: any;
  spe: any;
  prix1: any;
  prix2: any;
  order: any;
  name: any;
  nbResto: number = 0;
  url = this.apiService.url;
  urlImg = this.apiService.urlImg;
  param: string;
  showCriteria = false;
  icon = "ios-add-circle-outline";
  listSpeciality: any
  datasRestoLocal: any;

  d_city: boolean = false;
  d_name: boolean = false;
  d_speciality: boolean = false;
  getDatas: any;

  constructor(
    public events2: Events,
    public apiService: ApiService,
    private keyboard: Keyboard,
    private activatedRoute: ActivatedRoute,
    public events: Events,
    public loadingController: LoadingController,
  ) {
    this.speciality = activatedRoute.snapshot.paramMap.get('speciality');
    this.getDataFromSearch();
  }

  ngOnInit() {
    this.loadData();
  }

  async loadData(): Promise<void> {
    const loading = await this.loadingController.create({
      cssClass: 'customLoadClass',
      message: "chargement",
      spinner: 'dots'
    });
    await loading.present();
    this.keyboard.hide();
    this.getSpecialityList();
    loading.dismiss();
  }

  ionViewDidLeave() {
    !this.speciality ? this.speciality = "" : '';
    this.events.publish('previous', this.speciality);
  }

  setSearch() {
    if (this.datasRestoLocal) {
      this.datasRestoLocal = this.dataList;
      if (this.name) {
        this.datasRestoLocal = this.datasRestoLocal.filter(d => (d.nom_restau).toLowerCase().indexOf(this.name.toLowerCase()) !== -1);
      }
      if (this.speciality) {
        this.datasRestoLocal = this.datasRestoLocal.filter(d => d.nom_specialite == this.speciality);
      }
      if (this.order == -1) {
        this.datasRestoLocal = this.datasRestoLocal.sort((a, b) => {
          return b.id_restaurant - a.id_restaurant;
        });
      }
      if (this.order == 1) {
        this.datasRestoLocal = this.datasRestoLocal.sort((a, b) => {
          return a.id_restaurant - b.id_restaurant;
        });
      }
      if (this.city) {
        this.datasRestoLocal = this.datasRestoLocal.filter(d => (d.ville).toLowerCase().indexOf(this.city.toLowerCase()) !== -1);
      }
      this.nbResto = this.datasRestoLocal ? this.datasRestoLocal.length : 0;
    }
  }

  toggleDetails() {
    if (this.showCriteria) {
      this.showCriteria = false;
      this.icon = "ios-add-circle-outline";
    } else {
      this.showCriteria = true;
      this.icon = "ios-remove-circle-outline";
    }
  }

  getDataFromSearch() {
    this.events2.subscribe('search', (data) => {
      this.getDatas = data;
      this.getResto();
      this.events2.unsubscribe('search');
    });
  }

  getResto() {
    this.speciality == "" ? this.speciality = undefined : '';
    this.speciality ? this.datasReceive = this.speciality : this.datasReceive = this.getDatas;
    this.speciality ? this.param = 'speciality' : this.param = 'city';
    if (!Array.isArray(this.getDatas)) {
      this.getDatas ? this.city = this.getDatas : this.city = null;
      this.apiService.searchByCriteria({ [this.param]: this.datasReceive }).subscribe((data: any) => {
        this.dataList = data.data;
        this.datasRestoLocal = this.dataList;
        this.nbResto = (data.http == 200) ? data.data.length : 0;
      });
    }
    else {
      this.city = this.getDatas[0].city ? this.getDatas[0].city : null;
      this.speciality = this.getDatas[0].speciality ? this.getDatas[0].speciality : null;
      this.prix1 = this.getDatas[0].prix1 ? this.getDatas[0].prix1 : null;
      this.prix2 = this.getDatas[0].prix2 ? this.getDatas[0].prix2 : null;
      this.order = this.getDatas[0].order ? this.getDatas[0].order : null;
      this.name = this.getDatas[0].name ? this.getDatas[0].name : null;
      let datas = {
        'city': this.city,
        'speciality': this.speciality,
        'prix1': this.prix1,
        'prix2': this.prix2,
        'order': this.order,
        'name': this.name,
      };
      this.apiService.searchByCriteria(datas).subscribe((data: any) => {
        this.dataList = data.data;
        this.datasRestoLocal = this.dataList;
        this.nbResto = (data.http == 200) ? data.data.length : 0;
      });
    }
    /**
     * disabled fileds after search if set value
     */
    if (this.dataList) {
      this.city ? this.d_city = true : '';
      this.name ? this.d_name = true : '';
      this.speciality ? this.d_speciality = true : '';
    }
  }

  getSpecialityList() {
    this.apiService.getAllSpeciality().subscribe((response: any) => {
      this.listSpeciality = response.data;
    })
  };

}
