import { Component, OnInit } from '@angular/core';
import { ModalController, Events } from '@ionic/angular';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/rest/rest.service';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.page.html',
  styleUrls: ['./search-filter.page.scss'],
})
export class SearchFilterPage implements OnInit {
  public radiusmiles = 1;
  public minmaxprice = {
    upper: 100,
    lower: 5
  };
  searchDatas: string;
  city: string;
  specialite: string;
  organizeby: string;
  listSpeciality: [];
  name: string;

  constructor(
    private modalCtrl: ModalController,
    public events1: Events,
    public router: Router,
    public apiService: ApiService,
  ) { }

  ngOnInit() {
    this.getSpecialityList();
  }

  ionViewDidLeave() {
    let params = [{
      'city': this.city,
      'speciality': this.specialite,
      'prix1': this.minmaxprice.lower,
      'prix2': this.minmaxprice.upper,
      'order': this.organizeby,
      'name': this.name,
    }];
    this.events1.publish('search', params);
  }

  setSearch() {
    this.modalCtrl.dismiss();
    this.router.navigate(['/result-search' , ""]);
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  getSpecialityList() {
    this.apiService.getAllSpeciality().subscribe((response: any) => {
      this.listSpeciality = response.data;
    })
  };

}
