import { Component, ViewChild, OnInit } from '@angular/core';
import {
  NavController,
  MenuController,
  ModalController
} from '@ionic/angular';
// Modals
import { ApiService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';
import { Events } from '@ionic/angular';
import { Router } from '@angular/router';
import { DescriptionPage } from '../../restaurant/description/description.page';
import { bubble } from '../../../services/animations/animations'

@Component({
  selector: 'app-top-restaurant',
  templateUrl: './top-restaurant.page.html',
  styleUrls: ['./top-restaurant.page.scss'],
  animations: [bubble]
})
export class TopRestaurantPage implements OnInit {
  url = this.apiService.url;
  urlImg = this.apiService.urlImg;
  currentId: number;
  dataList = [];

  ngOnInit() {
    this.storage.get('USER_INFO').then((response) => {
      this.currentId = response[0].id;
      this.displayResto();
    });
  }

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    private apiService: ApiService,
    public storage: Storage,
    public events1: Events,
  ) {

  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }


  settings() {
    this.navCtrl.navigateForward('settings');
  }


  displayResto() {
    this.apiService.getTopResto().subscribe((data: any) => {
      this.dataList = data.data;
    });
  }

  async openDescription(id: number, nom: string) {
    const modal = await this.modalCtrl.create({
      component: DescriptionPage,
      componentProps: { idresto: id, nomresto: nom }
    });
    modal.onDidDismiss().then(() => {
      this.displayResto();
    });
    return await modal.present();
  }

  removeFav(id: number) {
    var datas = {
      'resto_id': id,
      'user_id': this.currentId,
    };
    this.apiService.removeToFavorite(datas).subscribe((data) => {
      this.displayResto();
    });
  }

}
