import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { FunctionService } from 'src/app/services/function/function.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/authentification/auth.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  public onUpdateForm: FormGroup;

  currentId: number;
  datas: any;
  nom: string;
  prenom: string;
  username: string;
  telephone: any;
  email: string;
  ville: string;
  token: string;

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public ApiService: ApiService,
    public storage: Storage,
    public functionService: FunctionService,
    public loadingController: LoadingController,
    public route: Router,
    private authService: AuthService
  ) {
    this.loadData();
  }

  async loadData(): Promise<void> {
    const loading = await this.loadingController.create({
      cssClass: 'customLoadClass',
      message: "chargement",
      spinner: 'dots'
    });
    await loading.present();
    this.storage.get('USER_INFO').then((response) => {
      this.currentId = response[0].id;
      this.token = response[0].token;
      if (this.datas) { this.getUserInfo(); }
      this.getUserInfo();
      loading.dismiss();
    });
  }

  ngOnInit() {
    this.onUpdateForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)
      ])],
      'telephone': [null, Validators.compose([
        Validators.required
      ])],
      'ville': [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  getUserInfo() {
    this.ApiService.getUser(this.currentId, this.token).subscribe((response: any) => {
      console.log(response);
      this.datas = response.data[0];
      this.nom = response.data[0].nom;
      this.prenom = response.data[0].prenom;
      this.username = response.data[0].username;
      this.email = response.data[0].mail;
      this.telephone = response.data[0].telephone;
      this.ville = response.data[0].ville;
    });
  }

  async sendData() {
    let params = {
      'id': this.currentId,
      'mail': this.email,
      'telephone': this.telephone,
      'ville_domicile': this.ville,
      'token': this.token,
    }
    this.ApiService.updateUser(params).subscribe((data: any) => {
      switch (data.status) {
        case 1:
          this.functionService.presentToast("Vos données ont bien été modifiées.");
          this.getUserInfo();
          break;
        case 99:
          this.authService.logout();
          this.functionService.presentToast('Une autre session à été ouverte sur un autre appareil. Vous avez été déconnecter.');
          break;
        default:
          this.functionService.presentToast("Une erreur s'est produite lors de la modification de vos données.");
          break;
      }
    });
  }

  updatePassword() {
    this.route.navigate(['update-password']);
  }
}
