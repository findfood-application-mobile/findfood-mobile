import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/rest/rest.service';
import { FunctionService } from 'src/app/services/function/function.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  email: string;
  username: string;
  newpassword: string;

  constructor(
    public apiService: ApiService,
    public functionService: FunctionService,
    public router: Router,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
  }

  checkForm() {
    let check = false;
    this.email && this.username ? check = true : check = false;
    return check;
  }

  setForm() {
    if(this.checkForm()) {
    let param = {
      'username': this.username,
      'email': this.email
    }
    this.apiService.forgotPassword(param).subscribe((resp: any) => {
      this.newpassword = resp.password;
      switch (resp.status) {
        case 1:
          this.sendMail();
          break;
        case 2:
          this.functionService.presentToast("Erreur : Invalide email/username");
          break;
        default:
          this.functionService.presentToast("Une erreur s'est produite. Verifier votre connexion.");
          break;
      }
    });
    } else {
      this.functionService.presentToast("Veuillez remplir tout les champs.");
    }
  }

  async sendMail(): Promise<void> {
    const loading = await this.loadingController.create({
      cssClass:'customLoadClass',
      message: "chargement",
      spinner: 'dots'
    });
    let param = { 'email': this.email, 'username': this.username, 'password': this.newpassword, 'type': 'forgot-password' }
    await loading.present();
    this.apiService.sendMail(param).subscribe((resp: any) => {
      console.log(resp);
      loading.dismiss();
      this.router.navigate(["/login"]);
      this.functionService.presentToast("Mot de passe réinitialiser avec succès. Verifier votre boite mail.");
    });
  }

}
