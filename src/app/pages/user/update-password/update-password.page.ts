import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/rest/rest.service';
import { FunctionService } from 'src/app/services/function/function.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.page.html',
  styleUrls: ['./update-password.page.scss'],
})
export class UpdatePasswordPage implements OnInit {

  lastpassword: string;
  password1: string;
  password2: string;
  iduser: string;
  token: string;
  msg: string;
  username: string;

  constructor(
    public apiService: ApiService,
    public functionService: FunctionService,
    public router: Router,
    public loadingController: LoadingController,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo() {
    this.storage.get('USER_INFO').then((resp) => {
      this.iduser = resp[0].id;
      this.token = resp[0].token;
      this.username = resp[0].username;
    });
  }

  checkForm() {
    let check = false;
    if (this.password1 && this.password2 && this.lastpassword) {
      if (this.password1 == this.password2 && this.password1.length < 7) {
        this.msg = 'Votre mot de passe doit comporter au moins 8 caractères.';
      }
      else if (this.password1 != this.password2) {
        this.msg = 'Vos mot de passe ne correspondent pas.';
      }
      else {
        check = true;
      }
    } else {
      this.msg = "Veuillez remplir tout les champs."
    }
    return check;
  }

  async setForm() {
    const loading = await this.loadingController.create({
      cssClass:'customLoadClass',
      message: "chargement",
      spinner: 'dots'
    });
    if (this.checkForm()) {
      await loading.present();
      let param = {
        'username': this.username,
        'newpassword': this.password1,
        'lastpassword': this.lastpassword,
        'id': this.iduser,
        'token': this.token
      }
      this.apiService.updatePassword(param).subscribe((resp: any) => {
        console.log(resp);
        switch (resp.status) {
          case 1:
            this.functionService.presentToast("Votre mot de passe à bien été modifier.");
            this.router.navigate(["/home"]);
            break;
          case 2:
            this.functionService.presentToast("Erreur : Votre mot de passe actuel est incorrect.");
            break;
          default:
            this.functionService.presentToast("Une erreur s'est produite. Verifier votre connexion.");
            break;
        }
      });
    } else {
      this.functionService.presentToast(this.msg);
    }
    loading.dismiss();
  }

}
