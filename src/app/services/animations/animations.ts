import { trigger, transition, style, animate, state } from '@angular/animations';

export const listderoul = trigger(
    'animat',
    [
        transition(
            ':enter',
            [
                style({ height: 0, opacity: 0 }),
                animate('0.5s ease-out',
                    style({ height: 145, opacity: 1 }))
            ]
        )
    ]);

export const bubble = trigger('items', [
    transition(':enter', [
        style({ transform: 'scale(0.5)', opacity: 0 }),
        animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)',
            style({ transform: 'scale(1)', opacity: 1 }))
    ])
]);

export const fadeout =  trigger('animat', [
    state('void', style({
      opacity: 0
    })),
    transition('void <=> *', animate(500)),
  ]);

  export const move = trigger('move', [
    state('flyIn', style({ transform: 'translateX(0)' })),
    transition(':enter', [
      style({ transform: 'translateX(-100%)' }),
      animate('0.5s 300ms ease-in')
    ]),
    transition(':leave', [
      animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
    ])
  ])