import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { ToastController, Platform } from '@ionic/angular';
import { FunctionService } from '../function/function.service';
import { ApiService } from '../rest/rest.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState = new BehaviorSubject(false);

  constructor(
    private router: Router,
    private storage: Storage,
    private platform: Platform,
    public toastController: ToastController,
    public services: FunctionService,
    public apiService: ApiService,
  ) {
    this.platform.ready().then(() => {
      this.checkLogged("startApp");
    });
  }

  /**
   * check if user loged & token valid when start app
   */
  checkLogged(start?) {
    this.storage.get('USER_INFO').then((response) => {
      if(response) {
      this.apiService.checkToken(response[0].id, response[0].token).subscribe((resp: any) => {
        if (response && resp.status == 1) {
          start ? this.authState.next(true) : '';
        } else {
          this.services.presentToast('Une autre session à été ouverte sur un autre appareil. Vous avez été déconnecter.');
          this.logout();
        }
      });
    }});
  }

  login(dataUser: any) {
    this.storage.set('USER_INFO', dataUser).then((response) => {
      this.router.navigate(['/home']);
      this.authState.next(true);
    });
  }

  logout() {
    this.storage.clear();
    this.router.navigate(['login']);
    this.authState.next(false);
  }

  isAuthenticated() {
    return this.authState.value;
  }

}
