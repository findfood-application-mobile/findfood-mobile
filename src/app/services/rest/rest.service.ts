import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public url = 'http://localhost/api_findfood_V2/index.php';
  public urlImg = 'http://localhost/api_findfood_V2/assets/img/';

  constructor(private httpClient: HttpClient) { }

  connection(params) {
    return this.httpClient.post(this.url + `?routes=user&action=login`, params);
  }

  getRest() {
    return this.httpClient.get(this.url + `?routes=restaurant&action=read`);
  }

  getFavorite(currentId: number) {
    return this.httpClient.get(this.url + `?routes=restaurant&action=readfavorite&id=` + currentId);
  }

  getUser(currentId: number, token: any) {
    return this.httpClient.get(this.url + `?routes=user&action=read&id=` + currentId + `&token=` + token);
  }

  updateUser(body: any) {
    return this.httpClient.put(this.url + `?routes=user&action=update`, body);
  }

  addToFavorite(params) {
    return this.httpClient.post(this.url + `?routes=restaurant&action=addfavorite`, params);
  }

  searchByCriteria(params) {
    return this.httpClient.post(this.url + `?routes=restaurant&action=search`, params);
  }

  removeToFavorite(params) {
    return this.httpClient.post(this.url + `?routes=restaurant&action=removefavorite`, params);
  }

  getAllSpeciality() {
    return this.httpClient.get(this.url + `?routes=speciality&action=read`);
  }

  registerUser(params) {
    return this.httpClient.post(this.url + `?routes=user&action=insert`, params);
  }

  getRestoById(id) {
    return this.httpClient.get(this.url + `?routes=restaurant&action=read&id=` + id);
  }

  getIdFavorite(id) {
    return this.httpClient.get(this.url + `?routes=restaurant&action=readidfavorite&id=` + id);
  }

  getTopResto() {
    return this.httpClient.get(this.url + `?routes=restaurant&action=top10`);
  }

  checkVote(params) {
    return this.httpClient.post(this.url + `?routes=vote&action=check`, params);
  }

  addVote(params) {
    return this.httpClient.post(this.url + `?routes=vote&action=insert`, params);
  }

  getCommentByResto(id) {
    return this.httpClient.get(this.url + `?routes=comment&action=read&id=` + id);
  }

  setComment(param) {
    return this.httpClient.post(this.url + `?routes=comment&action=insert`, param);
  }

  deleteComment(idcomment, id, token) {
    return this.httpClient.delete(this.url + `?routes=comment&action=delete&idcomment=` + idcomment + `&id=` + id + `&token=` + token);
  }

  addRestaurant(param) {
    return this.httpClient.post(this.url + `?routes=restaurant&action=insert`, param);
  }

  uploadFile(param) {
    return this.httpClient.post(this.url + `?routes=function&action=mobile-upload`, param);
  }

  getLocation(lat, lng) {
    return this.httpClient.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=AIzaSyBtG8dM2NsBBgorNzPO14IxA-qc0drEE9U");
  }

  getCityByCp(param) {
    return this.httpClient.post(this.url + `?routes=city&action=bycp`, param);
  }

  checkToken(id: number, token: String) {
    return this.httpClient.get(this.url + `?routes=function&action=check-token&id=` + id + `&token=` + token);
  }

  sendMail(param) {
    return this.httpClient.post(this.url + `?routes=function&action=send-mail`, param);
  }

  forgotPassword(param) {
    return this.httpClient.post(this.url + `?routes=user&action=forgot-password`, param);
  }
  updatePassword(param) {
    return this.httpClient.post(this.url + `?routes=user&action=update-password`, param);
  }

}
